extends Node2D

#scenes
const ferret = preload("res://characters/ferret.tscn")
signal toogle_freeze(freeze)
@onready var DeathShader = preload("res://res/grizzle.gdshader")
@onready var fps_label = get_node('fps_label')
@onready var ferret_timer = get_node('ferret_timer')
@onready var player = get_node("HeroMan")
@onready var shop_carpet = get_node("shop_rug") 
@onready var spawn_carpet = get_node("Spawn_rug/spawn_rug_sprite") 

var ferrets = []
var round_end = false

func _ready():
	DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_HIDDEN)
	
func fps_style(fps):
	return ("[b][color=lawngreen][font_size=30]" + str(fps) + "[/font_size][/color] FPS[/b]")

func _process(delta):
	var fps = 1.0 / delta
	fps_label.set_text(fps_style(fps))
	#if Input.is_action_just_pressed("key_ferret"):
	#	_spawn_ferret()
	if Input.is_action_just_pressed("quit_game"):
		get_tree().quit()
				
func _spawn_ferret():	
	if(not round_end):
		var ferret_instance = ferret.instantiate()
		var rand_x = (randi() % 1500 + 100)
		var rand_y = (randi() % 900 + 100)
		var ferret_position = Vector2(rand_x,rand_y)
		ferrets.append(ferret_instance)
		ferret_instance.position = ferret_position
		add_child(ferret_instance)
		ferret_instance.transform = Transform2D(ferret_position.angle_to_point(player.position),ferret_position)

func _on_ferret_timer_timeout():
	_spawn_ferret()

func _on_button_toggled(button_pressed):
	if(button_pressed):
		ferret_timer.start()
	else:
		ferret_timer.stop()

func _on_spawn_rug_body_entered(body):	
	if body.is_in_group("player"):
		ferret_timer.stop()
		freeze_ferrets(true)
	if body.is_in_group("ferret"):
		#body.stop()
		body.bounce_back()

func _on_spawn_rug_body_exited(body):
	if body.is_in_group("player") and not round_end:		
		ferret_timer.start()
		freeze_ferrets(false)


func freeze_ferrets(frozen):
	for f in ferrets:
			if(f==null):
				ferrets.erase(f)
			elif not round_end : 
				f.freeze(frozen)

func _on_hero_man_die():
	spawn_carpet.material.shader = DeathShader
	shop_carpet.material.shader = DeathShader
	freeze_ferrets(true)
	round_end = true
