extends Area2D
class_name GolfBall

@onready var label = get_node("score_label")
@onready var score = get_tree().get_root().get_node("Level/score_num")

var speed: float = 10
var delta_sum = 0.0
var kill_streak = 0

func _physics_process(_delta):
	position += transform.x * speed

func _on_body_shape_entered(_body_rid, body, _body_shape, _local_shape_index):
	kill_streak+=1
	var color = ""
	if(kill_streak == 2):
		color = "[color=salmon]"
	elif(kill_streak == 3):
		color = "[color=lawngreen]"
	else:
		color = "[color=hotpink]"
	label.text = color + "[font_size=20] " + str(kill_streak) + " [/font_size][/color]"
	var new_score = int(score.text.replace("[font_size=40]","").replace("[/font_size]","")) + 1
	new_score = "[font_size=40]" + str(new_score) + "[/font_size]"
	score.text = new_score
	body.queue_free()

"""
[color=salmon] [font_size=12] 5 [/font_size][/color]

"""
