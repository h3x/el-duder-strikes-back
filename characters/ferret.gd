extends CharacterBody2D

@onready var player = get_tree().get_root().get_node("Level/HeroMan")
@onready var label = get_node("ferret_label")

@export var speed: float = 1
var player_dist : float = 0.0
var frozen = false
func _ready():
	add_to_group("ferret")
	
func freeze(is_frozen):
	frozen= is_frozen
	
func _process(_delta):
	transform = Transform2D(self.position.angle_to_point(player.position),self.position)

func _physics_process(_delta):
	transform = Transform2D(self.position.angle_to_point(player.position),self.position)
	player_dist = self.position.distance_to(player.position)
	label.text = (str(int(player_dist)))	
	if not frozen :		
		position += transform.x * speed
	
func bounce_back():
	position -= (transform.x * speed) * 80
	
func _on_area_2d_body_entered(body):
	print(body)
	#self.queue_free()
