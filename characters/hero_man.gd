extends CharacterBody2D

#signals
signal hp_updated(hp)
signal die()

#fetch
@onready var anim_tree 		= $AnimationTree
@onready var sprite 		= $hero_sprite
@onready var player_bar 	= $HBoxContainer/HBoxContainer
@onready var ball_indicator	= $HBoxContainer/HBoxContainer/ball_indicator 
@onready var hp_bar 		= $HBoxContainer/HBoxContainer/hp_bar
@onready var bowl_timer 		= $bowl_timer
@onready var ferret_timer = get_tree().get_root().get_node("Level/ferret_timer")
@onready var anim_mode 		= anim_tree.get("parameters/playback")
@onready var hp 			= 5
@onready var DeathShader = preload("res://res/grizzle.gdshader")

#exports
@export var move_speed:float 		= 100.0
@export var spawn_direction:Vector2	= Vector2(0,1)
@export  var max_hp :float 			= 5.0
@export var num_balls:int 			= 400
@export var ball_cd:float 			= 0.2

#scenes
const ballPath = preload("res://res/golf_ball.tscn")

var oom 			= false
var speed 			= 0
var direction 		= Vector2(0,0)
var ball_on_cd 		= false
var current_ball_cd	= ball_cd
var dead = false

func _ready():
	update_animation(spawn_direction)
	add_to_group("player")
	
func _physics_process(_delta):
	# get change in x and y
	var input_dir = Vector2( Input.get_action_strength("right") - Input.get_action_strength("left"), Input.get_action_strength("down") - Input.get_action_strength("up"))
	direction = input_dir
	handle_input()
	handle_projectiles()
	update_animation(input_dir)
	velocity = input_dir * move_speed
	move_and_slide()
	check_collisions()

func handle_projectiles():
	# show ball indicator, when next ball ready
	if not ball_on_cd and num_balls > 0 and not dead:		
		ball_indicator.self_modulate = Color(1, 1, 1, 0.5)
	# hide ball indicator, when out of balls
	if(num_balls == 0 and not oom):
		player_bar.remove_child(ball_indicator)
		oom = true
		
func check_collisions():
	# check number of collisions
	var checked_colls = []
	for idx in get_slide_collision_count():
		# grab single collisions
		var collider = get_slide_collision(idx).get_collider()
		var collider_id = get_slide_collision(idx).get_collider_id()
		#check for ferret
		if(collider.is_in_group("ferret")):			
			if(collider_id not in checked_colls):
				checked_colls.append(collider_id)			
				collider.queue_free() # kill ferret
				take_damage() # take player damage

func update_animation(input_vec: Vector2):
	if(input_vec != Vector2.ZERO): # movement > 0
		anim_mode.travel("Walk")
	else: # keine bewegung
		speed = 0
		anim_mode.travel("Idle")
	anim_tree.set("parameters/Walk/blend_position", input_vec)
	anim_tree.set("parameters/Idle/blend_position", input_vec)

func take_damage():
	_set_hp(hp-1)
	
func _set_hp(hp_value):
	var prev_hp = hp
	hp = clamp(hp_value,0, max_hp)
	if(hp != prev_hp):
		emit_signal("hp_updated", hp)
		if(hp==0):
			death()			
			ferret_timer.stop()

func bowl():
	bowl_timer.start()
	if(num_balls > 0 and !ball_on_cd):
		var inst = ballPath.instantiate() 		# make ball
		var floor_pos = Vector2(position.x, position.y + 100)
		inst.position = floor_pos 				# position above floor
		owner.add_child(inst) # add to world		
		var angle = 90 + (-(direction.x) * 90) 	# very fucked up way to convert move vector2 to degree
		if(direction.x < 0):
			angle += (-direction.y * 45)
		elif(direction.x > 0):
			angle += direction.y * 45
		else:
			angle = (direction.y * 90) 			# back to normal
		inst.transform = Transform2D(deg_to_rad(angle),floor_pos)
		#num_balls-=1 # commented out = infinite balls
		ball_on_cd = true
	if(num_balls > 1): # make red (on cd)		
		ball_indicator.self_modulate = Color(1, 0, 0, 0.2)
	else: # make invisible (no cd)
		ball_indicator.self_modulate = Color(0, 0, 0, 0.0)

func handle_input():
	if not dead:
		if Input.is_action_just_pressed("keyQ"):
			bowl()
		
func death():
	dead = true
	sprite.material.shader = DeathShader	
	bowl_timer.stop()
	emit_signal("die")
	ball_indicator.self_modulate = Color(0, 0, 0, 0.0)
	print(ball_indicator)
	
func _on_bowl_timer_timeout():
	if not dead:
		ball_on_cd = false # reset cooldown
		ball_indicator.self_modulate = Color(1, 1, 1, 0.5) # remove red taint
